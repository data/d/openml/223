# OpenML dataset: stock

https://www.openml.org/d/223

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

This is a dataset obtained from the StatLib repository. Here is the included description:

 The data provided are daily stock prices from January 1988 through October 1991, for ten aerospace companies.

 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Original source: StatLib repository. 
 Characteristics: 950 cases, 10 continuous attributes

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/223) of an [OpenML dataset](https://www.openml.org/d/223). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/223/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/223/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/223/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

